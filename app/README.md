# TEST ASSIGNMENT FOR FRONT-END INTERNSHIP IN MOONCASCADE
#### SUMMER 2018

The aim of the test assignment is to create a single-page weather app.

## Deploying app
1. yarn install
2. cd app && yarn start

## Info
Although the requirements are filled,
some bugs will occur in the UX and the design is not fully implemented.
