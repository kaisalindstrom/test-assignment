import styled from 'styled-components';
import mcLogo from '../icons/mc-logo.png';

const Header = styled.div`
    background-color: #000000;
    width: 100%;
    height: 4.5rem;
    padding: .5rem;
    background: #000000 url(${mcLogo}) no-repeat center;
    background-size: 10rem
`;

export default Header;