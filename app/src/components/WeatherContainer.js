import styled from 'styled-components';

const WeatherContainer = styled.div`
    display:flex;
    flex-direction: row;
    border-radius: .25rem;
    justify-content: space-around;
`;

export default WeatherContainer;