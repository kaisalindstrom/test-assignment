import styled from 'styled-components';

const InfoContainer = styled.div`
    border-radius: .25rem;
    width: 33.25rem;
    background-color: #FFFFFF;
    font-size: 1.15rem;
`;

export default InfoContainer;