import React, {Component} from 'react';
import styled from 'styled-components';
import c from '../icons/weatherIcons/c.svg';
import h from '../icons/weatherIcons/h.svg';
import hc from '../icons/weatherIcons/hc.svg';
import hr from '../icons/weatherIcons/hr.svg';
import lc from '../icons/weatherIcons/lc.svg';
import lr from '../icons/weatherIcons/lr.svg';
import s from '../icons/weatherIcons/s.svg';
import sl from '../icons/weatherIcons/sl.svg';
import sn from '../icons/weatherIcons/sn.svg';
import t from '../icons/weatherIcons/t.svg';

const DateTitle = styled.p`
    color: #49ACFF;
    font-weight: bold;
    font-size: 1.15rem;
`;

const RowInfo = styled.p `
    font-size: .9rem;
`;

class WeatherInfo extends Component {

    render() {
        function roundNumber(number) {
            return Math.round(number);
        }
        function formatDate(date) {
            let d = new Date(date);
            d.setHours(0,0,0,0);
            let today = new Date(new Date().setHours(0, 0, 0, 0));
            let tomorrow = new Date(today);
            tomorrow.setDate(today.getDate()+1);
            let formattedDate;
            if ( d > tomorrow) {
                formattedDate = d.toLocaleDateString('en-GB');
            } else {
                if (d < tomorrow){
                    formattedDate = 'Today';
                }
                else {
                    formattedDate = 'Tomorrow';
                }
            }
            return formattedDate;
        }

        function getImageName(weather) {
            switch(weather) {
                case 'Snow':        return sn;
                case 'Sleet':       return sl;
                case 'Hail':        return h;
                case 'Thunder':     return t;
                case 'Heavy Rain':  return hr;
                case 'Light Rain':  return lr;
                case 'Showers':     return s;
                case 'Heavy Cloud': return hc;
                case 'Light Cloud': return lc;
                case 'Clear':       return c;
                default:            return "";
            }
        }
        let imageUrl = getImageName(this.props.day.weather_state_name);

        return (
            <div>
                <DateTitle>{formatDate(this.props.day.applicable_date)}</DateTitle>
                <RowInfo><img src={imageUrl} alt="weather-icon" height={35}/>{this.props.day.weather_state_name}</RowInfo>
                <RowInfo>Max:     {roundNumber(this.props.day.max_temp)}&#8451;</RowInfo>
                <RowInfo>Min:     {roundNumber(this.props.day.min_temp)}&#8451;</RowInfo>
                <RowInfo>{roundNumber(this.props.day.wind_speed)}mph</RowInfo>
            </div>
        );
    }
}

export default WeatherInfo;