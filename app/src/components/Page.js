import styled from 'styled-components';
import urban from '../icons/urban.jpg';

const Page = styled.div`
    display: flex;
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    height: -webkit-fill-available;
    background: url(${urban}) no-repeat center center fixed;
    background-size:  cover;
    font-family: Titillium Web, sans-serif;
`;

export default Page;

