import pinLogo from '../icons/ic-pin.svg';
import styled from 'styled-components';

const Input = styled.input.attrs({
    onChange: props => props.onChange
})`
  font-weight: bold;
  width: 24rem;
  height: 1.5rem;
  border: thin solid #FFFFFF;
  border-radius: .25rem;
  font-weight: bold;
  font-size: 1.15rem;
  padding: 1em 4em;
  margin-top: 20em;
  background: #FFFFFF url(${pinLogo}) no-repeat 5%;
  background-size: 2.5rem
  &:focus, &:focus-within {
    outline: none;
    border: thin solid #F5C52C;
  }
`;

export default Input;