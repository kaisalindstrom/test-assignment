import React, {Component} from 'react';
import WeatherInfo from './WeatherInfo';
import WeatherContainer from './WeatherContainer';
import Link from './Link';

class Suggestions extends Component {
    constructor(props) {
        super(props);

        this.state = {
            weatherDetailed: [],
            searchAction: false
        }
    }

    performDetailedSearch(id) {
        fetch('http://internship-proxy.aw.ee:3001/location/' + id)
            .then(res => res.json())
            .then(data => {
                const cityWeather = data.consolidated_weather.slice(0, 3);
                let array = [];

                cityWeather.forEach((day) => {
                    const info = <WeatherInfo day={day}/>;
                    array.push(info);
                });

                this.setState({
                    weatherDetailed: array,
                    searchAction: true
                });
            });
    }

    showDetailedInfo() {
        this.performDetailedSearch(this.props.city.woeid);
    }

    render() {
        const{searchAction} = this.state;
        return (
            <div>
            {searchAction
                ? <WeatherContainer>{this.state.weatherDetailed}</WeatherContainer>
                : <Link onClick={this.showDetailedInfo.bind(this)}>{this.props.city.title}</Link>
            }
            </div>
        );
    }
}

export default Suggestions;