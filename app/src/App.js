import React, { Component } from 'react';
import Header from './components/Header';
import Input from './components/Input';
import Suggestions from './components/Suggestions';
import Page from './components/Page';
import InfoContainer from './components/InfoContainer';

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchTerm: '',
            listOpen: false,
            suggestions: [],
        };
    }

    performSearch(searchTerm) {
        if(searchTerm === '') {
            this.setState({
                suggestions: []
            })
        } else {
            if (searchTerm.length > 2) {
                fetch('http://internship-proxy.aw.ee:3001/location?query=' + searchTerm)
                    .then(res => res.json())
                    .then(json => {
                        let array = [];
                        json.forEach((city) => {
                            const cityInfo = <Suggestions city={city}/>;
                            array.push(cityInfo);
                        });
                        this.setState({
                            listOpen: true,
                            suggestions: array
                        })
                    })
            }
        }
    };

    searchChangeHandler(event) {
        const searchTerm = event.target.value;
        this.performSearch(searchTerm);
    }


    render() {
        const{listOpen} = this.state;
        return (
            <div>
                <Header/>
                <Page>
                    <Input placeholder="Search location..." onChange={this.searchChangeHandler.bind(this)} type="text"/>
                    {listOpen
                        ? <InfoContainer>{this.state.suggestions}</InfoContainer>
                        : ""
                     }
                </Page>
            </div>
        );
    }
}

export default App;
